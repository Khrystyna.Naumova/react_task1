import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { filteredCoursesData } from './helpers/authorConstructor';
import React, { useState } from 'react';
import CreateCourse from './components/CreateCourse/CreateCourse';

function App() {
  const [isCreateCourseVisible, setIsCreateCoursesVisible] = useState(false);
  const [courses, setCourses] = useState(filteredCoursesData);

  const onAddCourse = () => {
    setIsCreateCoursesVisible(true);
  };

  const onSaveCourseData = (inputCourseData) => {
    const newCourse = {
      ...inputCourseData,
      id: Math.random().toString(),
    };
    setIsCreateCoursesVisible(false);
    setCourses((prevCourses) => {
      return [...prevCourses, newCourse];
    });
  };

  return (
    <div>
      <Header />
      {!isCreateCourseVisible ? (
        <Courses courses={courses} onAddCourse={onAddCourse} />
      ) : (
        <CreateCourse onSaveCourse={onSaveCourseData} />
      )}
    </div>
  );
}

export default App;
