import { mockedCoursesList, mockedAuthorsList } from '../constants.js';

export const filteredCoursesData = mockedCoursesList.map((item) => {
  const authors = item.authors.map((authorId) => {
    const issetId = mockedAuthorsList.filter(
      (_author) => authorId === _author.id
    );
    if (issetId.length) {
      return issetId[0].name;
    }
    return authorId;
  });
  return {
    ...item,
    authors,
  };
});

export function authorArraySort(list) {
  const authorNames = list.map((author) => {
    return author.name;
  });
  return authorNames;
}

// console.log(filteredCoursesData)
