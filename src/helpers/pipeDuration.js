function pipeDuration(min) {
  const hours = min / 60; //2.66
  const rhours = Math.floor(hours); //2
  const minutes = (hours - rhours) * 60; //39.9
  const rminutes = Math.round(minutes); //30
  return `${rhours}:${rminutes} hours`;
}

export default pipeDuration;
