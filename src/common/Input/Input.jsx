import './Input.css';

function Input(props) {
  return (
    <div className='input'>
      <label htmlFor='inp'>{props.labelText}</label>
      <input
        placeholder={props.placeholderText}
        onChange={props.handleChange}
      />
    </div>
  );
}

export default Input;
