import './Button.css';

function Button(props) {
  return (
    <div>
      <button className='button' onClick={props.handleClick}>
        {props.buttonText}
      </button>
    </div>
  );
}

export default Button;
