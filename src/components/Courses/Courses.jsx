import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import './Courses.css';
import React, { useState } from 'react';

function Courses(props) {
  // Use two pieces of state; one to track the value in your text field and the other to store the search term for filtering. Only set the latter when you click your button

  const [searchValue, setSearchValue] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const filteredCourses = props.courses.filter((course) => {
    return course.title.toLowerCase().includes(searchTerm.toLowerCase());
  });

  const searchTitleHandler = (title) => {
    setSearchValue(title);
  };

  const onSearchButton = () => {
    setSearchTerm(searchValue);
  };

  return (
    <div className='courses'>
      <div className='searchBar'>
        <SearchBar
          title={searchValue}
          onChangeTitle={searchTitleHandler}
          onSearch={onSearchButton}
        />
        <Button buttonText='Add new course' handleClick={props.onAddCourse} />
      </div>
      {filteredCourses.length === 0 ? (
        <p>No matches courses.</p>
      ) : (
        filteredCourses.map((course) => (
          <CourseCard
            key={course.id}
            title={course.title}
            description={course.description}
            authors={course.authors}
            duration={course.duration}
            creationDate={course.creationDate}
          />
        ))
      )}
    </div>
  );
}

export default Courses;
