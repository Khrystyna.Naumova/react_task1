import Button from '../../../../common/Button/Button';
import './CourseCard.css';
import pipeDuration from '../../../../helpers/pipeDuration';

function CourseCard(props) {
  const duration = pipeDuration(props.duration);

  const authNames = props.authors.join(', ');

  return (
    <div className='courseCard'>
      <div className='courseCard__1'>
        <h2>{props.title}</h2>
        <p>{props.description}</p>
      </div>
      <div className='courseCard__2'>
        <p>
          <span>Authors:</span> {authNames}
        </p>
        <p>
          <span>Duration:</span> {duration}{' '}
        </p>
        <p>
          <span>Created:</span> {props.creationDate}
        </p>
        <Button buttonText='Show course' />
      </div>
    </div>
  );
}

export default CourseCard;
