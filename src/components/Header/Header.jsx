import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import './Header.css';

function Header() {
  return (
    <div className='header'>
      <Logo />
      <div className='headerIn'>
        <h4>Kristen</h4>
        <Button buttonText='Logout' />
      </div>
    </div>
  );
}

export default Header;
